
import openai
from flask import Flask, render_template, request, jsonify, send_file
from pymongo import MongoClient
from sentence_transformers import SentenceTransformer, util

app = Flask(__name__)

openai.api_key = "API_KEY"
model = SentenceTransformer("sentence-transformers/bert-base-nli-mean-tokens")


client = MongoClient("mongodb+srv://zaeemmohzar:Fih7Fjj0vgfDYIh4@cluster0.l69xdq2.mongodb.net/TeacherPrompt?retryWrites=true&w=majority")
db = client["TeacherPrompt"]
collection = db["mycollection"]

def get_similar_answer(transcribed_text):
    # Retrieve all documents from the database
    documents = collection.find()
    max_similarity = 0
    best_match = None
    transcribed_text_embedding = model.encode([transcribed_text])[0]
    for doc in documents:
        # Compute similarity between transcribed text and document transcription
        doc_embedding = model.encode([doc["transcription"]])[0]
        similarity = util.cos_sim(transcribed_text_embedding, doc_embedding)
        if similarity > max_similarity:
            max_similarity = similarity
            best_match = doc["answer"]
    if max_similarity >= 0.90:
        return best_match
    return None


@app.route("/logo.png")
def get_image():
    return send_file("logo.png")


@app.route("/")
def home():
    return render_template("index.html")

@app.route('/save-audio', methods=['POST'])
def save_audio():
    audio_file = request.files['audio']
    audio_file.save('audio_file.wav')  # Save the file to desired location
    return 'File uploaded successfully'



@app.route("/transcribe_audio", methods=["POST"])
def transcribe_audio():

    try:
        audio_file_path="audio_file.wav"
        with open(audio_file_path, "rb") as audio_file:
            transcribed_text = openai.Audio.transcribe("whisper-1", audio_file)

        transcribed_text_str = transcribed_text['text'] # corrected
        # Check if a similar question has been asked before and retrieve the corresponding answer from the database
        similar_answer = get_similar_answer(transcribed_text_str)
        if similar_answer is not None:
            # Use threading to run the speech and HTML rendering in separate threads

            return render_template(
                "index.html", transcription=transcribed_text_str, answer=similar_answer
            )
        else:
            # Use OpenAI API to answer the question
            prompt = f"What is {transcribed_text_str}?" # corrected
            response = openai.Completion.create(
                model="text-davinci-003",
                prompt=prompt,
                temperature=0.7,
                max_tokens=256,
                top_p=1,
                frequency_penalty=0,
                presence_penalty=0,
            )
            answer = response.choices[0].text
            answer_doc = {"transcription": transcribed_text_str, "answer": answer}
            collection.insert_one(answer_doc)
            return render_template(
                "index.html", transcription=transcribed_text_str, answer=answer
            )

    except Exception as e:
        return render_template("index.html", error=f"Error: {e}")

if __name__ == "_main_":
    app.run(host="0.0.0.0", port=5000, debug=True)