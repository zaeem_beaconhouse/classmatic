import speech_recognition as sr

# Create a function to listen to microphone input
def listen():
    # Create an instance of the Recognizer class
    r = sr.Recognizer()

    # Use the default microphone as the audio source
    with sr.Microphone() as source:
        print("Say something!")
        audio = r.listen(source)

    try:
        # Use the Google Speech Recognition API to transcribe audio to text
        text = r.recognize_google(audio)
        return text
    except sr.UnknownValueError:
        print("Sorry, I didn't understand that.")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))

# Listen for the keyword "hello bot"
while True:
    text = listen()
    if text == "hello bot":
        print("Hello! How can I help you?")
        break
